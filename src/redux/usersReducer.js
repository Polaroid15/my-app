const FOLLOW = "follow";
const UNFOLLOW = "unfollow";
const SET_USERS = "set_users";

let initialState = {
    users: [
        {
            id: 1,
            name: 'Megan Fox',
            status: 'status 1',
            ava: 'https://i.ytimg.com/vi/nmtg1hVjR9k/maxresdefault.jpg',
            location: {country: 'Russia', city: 'Moscow'},
            isFollow: false
        },
        {
            id: 2,
            name: 'Beyonce',
            status: 'status 2',
            ava: 'http://www.spletnik.ru/thumb/310x310/img/persons/beyonce_post1.jpg',
            location: {country: 'Great Britain', city: 'London'},
            isFollow: true
        },
        {
            id: 3,
            name: 'Salma Hayek',
            status: 'status 3',
            ava: 'https://see.news/wp-content/uploads/2019/09/1504186008-salma-hayek.jpg',
            location: {country: 'Germany', city: 'Berlin'},
            isFollow: false
        },
        {
            id: 4,
            name: 'Emma Stone',
            status: 'status 3',
            ava: 'http://thumbs.dfs.ivi.ru/storage15/contents/d/b/e02671b0fee9d075efece2a0d4289f.jpg',
            location: {country: 'America', city: 'Kansas'},
            isFollow: true
        },
        {
            id: 5,
            name: 'Margot Robbie',
            status: 'status 4',
            ava: 'https://cdn1-www.superherohype.com/assets/uploads/2019/02/Harley-e1550078715388.jpg',
            location: {country: 'Norway', city: 'Oslo'},
            isFollow: false
        },
    ]
};

const usersReducer = (state = initialState, action) => {

    switch (action.type) {
        case FOLLOW:
            return {
                ...state, users: state.users.map(user => {
                    if (user.id === action.userId) {
                        return {...user, isFollow: true};
                    }
                    return user;
                })
            };
        case UNFOLLOW:
            return {
                ...state, users: state.users.map(user => {
                    if (user.id === action.userId) {
                        return {...user, isFollow: false};
                    }
                    return user;
                })
            };
        case SET_USERS:
            return {
                ...state, users: [...state.users, action.users]
            };
        default:
            return state;
    }
};

export const followActionCreator = (userId) => ({type: FOLLOW, userId});
export const unfollowActionCreator = (userId) => ({type: UNFOLLOW, userId});
export const setUsersActionCreator = (users) => ({type: SET_USERS, users});

export default usersReducer;