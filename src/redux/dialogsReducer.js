const UPDATE_NEW_MESSAGE_BODY = "Update_new_message_body";
const SEND_MESSAGE = "Send_message";

let initialState = {
    dialogs: [
        {id: 1, name: 'Beyonce', ava: 'http://www.spletnik.ru/thumb/310x310/img/persons/beyonce_post1.jpg'},
        {id: 2, name: 'Megan Fox', ava: 'https://i.ytimg.com/vi/nmtg1hVjR9k/maxresdefault.jpg'},
        {
            id: 3,
            name: 'Emma Stone',
            ava: 'http://thumbs.dfs.ivi.ru/storage15/contents/d/b/e02671b0fee9d075efece2a0d4289f.jpg'
        },
        {
            id: 4,
            name: 'Margot Robbie',
            ava: 'https://cdn1-www.superherohype.com/assets/uploads/2019/02/Harley-e1550078715388.jpg'
        },
        {
            id: 5,
            name: 'Salma Hayek',
            ava: 'https://see.news/wp-content/uploads/2019/09/1504186008-salma-hayek.jpg'
        },
    ],
    messages: [
        {id: 1, message: 'Hail!', isSelf: true},
        {id: 2, message: 'Berlin, lets go!', isSelf: false},
        {id: 3, message: 'DAS IS FANTASTISCH', isSelf: true},
    ],
    newMessageBody: ""
};

const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_NEW_MESSAGE_BODY:
            return {
                ...state,
                newMessageBody: action.body
            };
        case SEND_MESSAGE:
            let body = state.newMessageBody;
            return {
                ...state,
                messages: [...state.messages, {id: 66, message: body}],
                newMessageBody: ""
            };
        default:
            return state;
    }
};

export const sendMessageCreator = () => ({type: SEND_MESSAGE});

export const updateNewMessageBodyCreator = (text) => ({type: UPDATE_NEW_MESSAGE_BODY, body: text});

export default dialogsReducer;