let initialState = {
    friends: [
        {id: 1, name: 'Megan Fox', ava: 'https://i.ytimg.com/vi/nmtg1hVjR9k/maxresdefault.jpg'},
        {id: 2, name: 'Beyonce', ava: 'http://www.spletnik.ru/thumb/310x310/img/persons/beyonce_post1.jpg'},
        {
            id: 3,
            name: 'Salma Hayek',
            ava: 'https://see.news/wp-content/uploads/2019/09/1504186008-salma-hayek.jpg'
        },
        {
            id: 4,
            name: 'Emma Stone',
            ava: 'http://thumbs.dfs.ivi.ru/storage15/contents/d/b/e02671b0fee9d075efece2a0d4289f.jpg'
        },
        {
            id: 5,
            name: 'Margot Robbie',
            ava: 'https://cdn1-www.superherohype.com/assets/uploads/2019/02/Harley-e1550078715388.jpg'
        },
    ]
};

const friendsReducer = (state = initialState, action) => {
    return state;
};

// export const sendMessageCreator = () => ({type: SEND_MESSAGE});

export default friendsReducer;