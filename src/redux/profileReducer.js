const ADD_POST = "Add_post";
const UPDATE_NEW_POST_TEXT = "Update_new_post_text";

let initialState = {
    posts: [
        {id: 1, message: 'Aloha, how u doing?', likesQuantity: 50},
        {id: 2, message: 'it\'s me', likesQuantity: 13},
    ],
    newPostText: 'Hello Ladies'
};

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST:
            let newPost = {
                id: 5,
                message: state.newPostText,
                likesQuantity: 0
            };
            return {
                ...state,
                posts: [...state.posts, newPost],
                newPostText: ""
            };
        case UPDATE_NEW_POST_TEXT:
            return {
                ...state,
                newPostText: action.newText
            };
        default:
            return state;
    }
};

export const addPostCreator = () => ({type: ADD_POST});

export const updatePostTextCreator = (text) => ({type: UPDATE_NEW_POST_TEXT, newText: text});

export default profileReducer;