import profileReducer from "./profileReducer";
import dialogsReducer from "./dialogsReducer";
import friendsReducer from "./friendsReducer";

let store = {
    _state: {
        profilePage: {
            posts: [
                {id: 1, message: 'Aloha, how u doing?', likesQuantity: 50},
                {id: 2, message: 'it\'s me', likesQuantity: 13},
            ],
            newPostText: 'Hello Ladies'
        },
        dialogsPage: {
            dialogs: [
                {id: 1, name: 'Beyonce', ava: 'http://www.spletnik.ru/thumb/310x310/img/persons/beyonce_post1.jpg'},
                {id: 2, name: 'Megan Fox', ava: 'https://i.ytimg.com/vi/nmtg1hVjR9k/maxresdefault.jpg'},
                {
                    id: 3,
                    name: 'Emma Stone',
                    ava: 'http://thumbs.dfs.ivi.ru/storage15/contents/d/b/e02671b0fee9d075efece2a0d4289f.jpg'
                },
                {
                    id: 4,
                    name: 'Margot Robbie',
                    ava: 'https://cdn1-www.superherohype.com/assets/uploads/2019/02/Harley-e1550078715388.jpg'
                },
                {
                    id: 5,
                    name: 'Salma Hayek',
                    ava: 'https://see.news/wp-content/uploads/2019/09/1504186008-salma-hayek.jpg'
                },
            ],
            messages: [
                {id: 1, message: 'Hail!', isSelf: true},
                {id: 2, message: 'Berlin, lets go!', isSelf: false},
                {id: 3, message: 'DAS IS FANTASTISCH', isSelf: true},
            ],
            newMessageBody: ""
        },
        friendsPage: {
            friends: [
                {id: 1, name: 'Megan Fox', ava: 'https://i.ytimg.com/vi/nmtg1hVjR9k/maxresdefault.jpg'},
                {id: 2, name: 'Beyonce', ava: 'http://www.spletnik.ru/thumb/310x310/img/persons/beyonce_post1.jpg'},
                {
                    id: 3,
                    name: 'Salma Hayek',
                    ava: 'https://see.news/wp-content/uploads/2019/09/1504186008-salma-hayek.jpg'
                },
                {
                    id: 4,
                    name: 'Emma Stone',
                    ava: 'http://thumbs.dfs.ivi.ru/storage15/contents/d/b/e02671b0fee9d075efece2a0d4289f.jpg'
                },
                {
                    id: 5,
                    name: 'Margot Robbie',
                    ava: 'https://cdn1-www.superherohype.com/assets/uploads/2019/02/Harley-e1550078715388.jpg'
                },
            ]
        }
    },
    _callSubscriber() {
        console.log("rerender base");
    },
    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer;
    },
    dispatch(action) {

        this._state.profilePage = profileReducer(this._state.profilePage, action);
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
        this._state.friendsPage = friendsReducer(this._state.friendsPage, action);
        this._state.usersPage = usersReducer(this._state.usersPage, action);
        this._callSubscriber(this._state);
    }
};

export default store;