import React from 'react';
import navStyle from './NavBar.module.css';
import {NavLink} from "react-router-dom";

const Nav = () => {
    return (
        <nav className={navStyle.nav}>
            <div className={`${navStyle.item} ${navStyle.active}`}>
                <NavLink to="/profile" activeClassName={navStyle.activeLink}> Profile </NavLink>
            </div>
            <div className={navStyle.item}>
                <NavLink to="/dialogs" activeClassName={navStyle.activeLink}> Messages</NavLink>
            </div>
            <div className={navStyle.item}>
                <NavLink to="/music" activeClassName={navStyle.activeLink}> Music</NavLink>
            </div>
            <div className={navStyle.item}>
                <NavLink to="/settings" activeClassName={navStyle.activeLink}> Settings</NavLink>
            </div>
            <div className={navStyle.item}>
                <NavLink to="/friends" activeClassName={navStyle.activeLink}> Friends</NavLink>
            </div>
            <div className={navStyle.item}>
                <NavLink to="/users" activeClassName={navStyle.activeLink}> Users</NavLink>
            </div>
        </nav>
    )
};

export default Nav;