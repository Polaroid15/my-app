import React from 'react';
import postStyle from './Post.module.css';

const Post = (props) => {
  
  return <div className={postStyle.item}>
    <img src='https://clutch.ua/crops/302d9c/360x0/1/0/2019/01/16/UGd5M9P5KAf5eMTnGjrCQUYjnfBuoENb.jpg' />
    {props.message}
    <div>
      <span>likes: {props.likesQuantity}</span>
    </div>
  </div>
};

export default Post;