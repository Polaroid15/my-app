import React from 'react';
import myPostsStyle from './MyPosts.module.css';
import Post from './Post/Post';

const MyPosts = (props) => {

    let newPostElement = React.createRef();
    let postsElements = props.posts.map(post => <Post message={post.message} likesQuantity={post.likesQuantity}/>);

    let onAddPost = (() => {
        props.addPost();
    });

    let onPostChange = () => {
      let text = newPostElement.current.value;
      props.updateNewPostText(text);
    };
    
    return (
        <div className={myPostsStyle.appContent}>
            <div className={myPostsStyle.contentWrapper}>
                <h3>My posts</h3>
                <div>
                    <div>
                        <textarea
                            ref={newPostElement}
                            onChange={onPostChange}
                            value={props.newPostText}/>
                    </div>
                    <div>
                        <button onClick={onAddPost}>Add Post</button>
                    </div>
                </div>
                <div className={myPostsStyle.posts}>
                    {postsElements}
                </div>
            </div>
        </div>
    )
};

export default MyPosts;