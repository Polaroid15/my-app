import React from 'react';
import messageStyle from './Message.module.css';

const Message = (props) => {
    return (
        <div className={messageStyle.messages} >
            {props.text}
        </div>
    )
};

export default Message;