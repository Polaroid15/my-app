import React from 'react';
import dialogStyle from './Dialog.module.css';
import {NavLink} from "react-router-dom";

const Dialog = (props) => {
    let path = "/dialogs/" + props.id;
    return (
        <div className={dialogStyle.dialog}>
            <NavLink to={path} activeClassName={dialogStyle.activeLink}>
                <img src={props.ava}/>
                {props.name}
            </NavLink>
        </div>
    )
};

export default Dialog;