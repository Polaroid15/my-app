import React from 'react';
import dialogStyle from './Dialogs.module.css';
import Dialog from './Dialog/Dialog'
import Message from './Message/Message'

const Dialogs = (props) => {

    let state = props.dialogsPage;
    let dialogsElements = state.dialogs.map(dialog => <Dialog name={dialog.name} id={dialog.id} ava={dialog.ava}/>);
    let messagesElements = state.messages.map(msg => <Message text={msg.message} isSelf={msg.isSelf}/>);

    let sendMessageClick = () => {
        props.sendMessage();
    };

    let onNewMessageChange = (e) => {
        let body = e.target.value;
        props.onNewMessageChange(body);
    };

    return (
        <div className={dialogStyle.dialogs}>
            <div className={dialogStyle.dialogsList}>
                {dialogsElements}
            </div>
            <div className={dialogStyle.messages}>
                <div>{messagesElements}</div>
                <div>
                    <textarea placeholder='Enter your message'
                              value={state.newMessageBody}
                              onChange={onNewMessageChange}/>
                </div>
                <div>
                    <button onClick={sendMessageClick}>Send message</button>
                </div>
            </div>
        </div>
    )
};

export default Dialogs;