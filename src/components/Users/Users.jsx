import React from 'react';
import usersStyle from "../Users/Users.module.css";
import User from "./User/User";

let Users = (props) => {
    
    let usersElements = props.users.map(user => <User user={user} follow={props.follow} unfollow={props.unfollow} />);

    return <div className={usersStyle.users}>
        <div className={usersStyle.usersList}>
            {usersElements}
        </div>
    </div>;
};

export default Users;