import React from 'react';
import userStyle from './User.module.css';
import {NavLink} from "react-router-dom";

const User = (props) => {    
    let user = props.user;
    let path = "/users/" + user.id;
    

    return (
        <div className={userStyle.user}>
            <NavLink to={path} activeClassName={userStyle.activeLink}>
                <div>
                    {user.name}
                </div>
                <div>
                    {user.isFollow
                       ? <button onClick={ () => {props.unfollow(user.id)}}>UnFollow</button>
                       : <button onClick={ () => {props.follow(user.id)}}>Follow</button>}
                </div>
                <div>
                    <img src={user.ava}/>
                </div>
            </NavLink>
        </div>
    )
};

export default User;