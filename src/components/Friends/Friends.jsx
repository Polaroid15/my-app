import React from 'react';
import friendsStyle from './Friends.module.css';
import Friend from "./Friend/Friend";

const Friends = (props) => {

  let state = props.friendsPage;
  let friendsElements = state.friends.map(friend => <Friend id={friend.id} name={friend.name} ava={friend.ava} />);

  return <div className={friendsStyle.friends}>
    <div className={friendsStyle.friendsList}>
      {friendsElements}
    </div>
  </div>
};

export default Friends;