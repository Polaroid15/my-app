import React from 'react';
import friendStyle from './Friend.module.css';
import {NavLink} from "react-router-dom";

const Friend = (props) => {
    let path = "/friends/" + props.id;

    return (
        <div className={friendStyle.friend}>
            <NavLink to={path} activeClassName={friendStyle.activeLink}>
                <div>
                    {props.name}
                </div>
                <div>
                    <img src={props.ava}/>
                </div>
            </NavLink>
        </div>
    )
};

export default Friend;