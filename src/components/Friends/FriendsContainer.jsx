import React from 'react';
import {addPostCreator, updatePostTextCreator} from "../../redux/friendsReducer";
import {connect} from "react-redux";
import Friends from "./Friends";

let mapStateToProps = (state) => {
    return {
        friendsPage: state.friendsPage
    }
};

let mapDispatchToProps = (dispatch) => {
    return {}
};

const FriendsContainer = connect(mapStateToProps, mapDispatchToProps)(Friends);

export default FriendsContainer;