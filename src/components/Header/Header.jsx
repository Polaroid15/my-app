import React from 'react';
import headerStyle from './Header.module.css';

const Header = (props) => {
    return <header className={headerStyle.appHeader}>
        <img src='http://buben.fm/img/artist/256/5206.png' alt='message if img not loaded' />
    </header>
};

export default Header;