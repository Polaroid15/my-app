import React from 'react';
import './App.css';
import {Route} from "react-router-dom";
import Header from './components/Header/Header';
import Nav from './components/Navbar/NavBar';

import Profile from './components/Profile/Profile';
import Settings from './components/Settings/Settings';
import Music from './components/Music/Music';
import DialogsContainer from "./components/Dialogs/DialogsContainer";
import FriendsContainer from "./components/Friends/FriendsContainer";
import UsersContainer from "./components/Users/UsersContainer";

function App() {

    return (
        <div className='app-wrapper'>
            <Header name='Shakira' age='42'/>
            <Nav/>
            <div className='app-wrapper-content'>
                <Route path="/profile" render={() => <Profile />}/>
                <Route path="/dialogs" render={() => <DialogsContainer />}/>
                <Route path="/music" component={Music}/>
                <Route path="/settings" component={Settings}/>
                <Route path="/users" render={ () => <UsersContainer/>}/>
            </div>
            <div className='app-wrapper-friends'>
                <Route path="/friends" render={() => <FriendsContainer />}/>
            </div>
        </div>
    );
}


export default App;
